;;; gmat-script-mode -- Emacs major mode for editing GMAT script syntax  -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 John Kastner
;;
;; Author: John Kastner
;; URL: https://gitlab.com/jkkastner/gmat-script-mode
;; Version: May 5, 2021
;; Package-Requires: ((emacs "25.1") (python-mode "0.26.1"))
;; Maintainer: john@kastner.us
;; Created: May 5, 2021
;; Keywords: languages, gmat
;; License: GNU General Public License Version 3
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; ‘gmat-script-mode’ is an Emacs major mode for editing GMAT (General
;; Mission Analsys Tool) script files. It provides syntax highlighting
;; and completion for keywords and special variables.
;;
;; For more information about GMAT, see URL
;; ‘http://gmat.sourceforge.net/docs/R2020a/help.html’ or
;; 'https://sourceforge.net/projects/gmat/'.  For details about the
;; GMAT script syntax and file format, see URL
;; ‘http://gmat.sourceforge.net/docs/R2020a/help.html#ScriptLanguage’.
;;
;; This work was inspired by:
;;    https://github.com/Rohan-Goyal/emacs-gmatscript
;;    https://github.com/rockstorm101/GMAT

;;; Future expansion suggestions:

;; - An interface to a GMAT file linter via ‘gmat-script-lint’.
;; - Syntax highlighting for a GMAT thrust history file.
;;      keywords: "Start_Epoch",
;;                "Thrust_Vector_Coordinate_System",
;;                "Thrust_Vector_Interpolation_Method",
;;                "Mass_Flow_Rate_Interpolation_Method"
;; - Syntax highlighting for GMAT tracking data types.
;;      keywords: "DSN_SeqRange",
;;                "DSN_TCP",
;;                "GPS_PosVec",
;;                "Range",
;;                "Range_Skin",
;;                "RangeRate",
;;                "SN_Range",
;;                "SN_Doppler",
;;                "Azimuth",
;;                "Elevation",
;;                "XEast",
;;                "YNorth",
;;                "XSouth",
;;                "YEast"


;;; Code:

;; Load Python mode in order to initialize a base syntax table.
;; was (require 'python-mode)
(autoload 'python-mode "python-mode" "Python Mode." t)

;; Define the GMAT keywords.
(defconst gmat-script-keywords
  (let*
      (
       (flow-control '("If"
                       "For"
                       "While"
                       "BeginScript"
                       "Target"
                       "Optimize"
                       "Else"))

       (flow-ends '("EndIf"
                    "EndFor"
                    "EndWhile"
                    "EndScript"
                    "EndTarget"
                    "EndOptimize"))

       (objects '("Array"
                  "Barycenter"
                  "CoordinateSystem"
                  "DifferentialCorrector"
                  "EphemerisFile"
                  "FileInterface"
                  "FiniteBurn"
                  "FminconOptimizer"
                  "ForceModel"
                  "Formation"
                  "FuelTank"
                  "GroundStation"
                  "GroundTrackPlot"
                  "ImpulsiveBurn"
                  "LibrationPoint"
                  "MatlabFunction"
                  "OrbitView"
                  "Propagator"
                  "ReportFile"
                  "SolarSystem"
                  "Spacecraft"
                  "String"
                  "Thruster"
                  "Variable"
                  "VF13ad"
                  "XYPlot"
                  "Asteroid"
                  "Comet"
                  "Planet"
                  "Moon"))

       (functions '("Achieve"
                    "BeginMissionSequence"
                    "BeginFiniteBurn"
                    "BeginScript"
                    "CallMatlabFunction"
                    "ClearPlot"
                    "Create"
                    "EndFiniteBurn"
                    "For"
                    "If"
                    "GMAT"
                    "Maneuver"
                    "MarkPoint"
                    "NonlinearConstraint"
                    "Optimize"
                    "PenDown"
                    "PenUp"
                    "Propagate"
                    "Report"
                    "Set"
                    "Stop"
                    "Target"
                    "Toggle"
                    "Vary"
                    "While"))

       (misc '("Yukon"
               "CommandEcho"
               "Include"
               "Write"
               "BatchEstimatorInv"
               "Simulator"
               "ErrorModel"
               "AcceptFilter"
               "RejectFilter"
               "TrackingDataSet"
               "RunEstimator"
               "RunSimulator"))

       (flow-control-regex (regexp-opt flow-control 'words))
       (flow-ends-regex (regexp-opt flow-ends 'words))
       (objects-regex (regexp-opt objects 'words))
       (functions-regex (regexp-opt functions 'words))
       (misc-regex (regexp-opt misc 'words))
       ) ; Generates regexes to match these wordlists
    `(
      (,flow-control-regex . font-lock-keyword-face)
      (,flow-ends-regex . font-lock-keyword-face)
      (,objects-regex . font-lock-type-face)
      (,misc-regex . font-lock-constant-face)
      (,functions-regex . font-lock-builtin-face)
      ))
  "GMAT script keywords")

;; Define the syntax table (based on the syntax table for Python).
(defconst gmat-script-mode-syntax-table
  (let ( (synTable (make-syntax-table python-mode-syntax-table)))
    ;; Set/modify each character's class
    (modify-syntax-entry ?% "<" synTable)
    (modify-syntax-entry ?\n ">" synTable) ; Modifies the comment syntax
                                           ; (modify-syntax-entry ?\' (string ?') synTable)
    synTable)
  "GMAT syntax table")

;;;###autoload
(define-generic-mode 'gmat-script-mode
  ;; comment-list
  '("%")

  ;; keyword-list
  (eval gmat-script-keywords)

  ;; Fonto-lock-list (extra stuff to colorize)
  '(("\[.*\]" . 'font-lock-builtin-face))

  ;; auto-mode-list (files to colorize based on extension)
  '("*.script$")

  ;; function-list
  nil

  ;; docstring
  "Mode for editing GMAT script files.")

;;;###autoload
(define-derived-mode gmat-script-mode js-jsx-mode "gmat mode"
  "Major mode for editing GMAT (General Mission Analysis Tool) scripts."

  ;; Code for syntax highlighting
  (set-syntax-table gmat-script-mode-syntax-table)
  (setq font-lock-defaults '((gmat-script-keywords))))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.script\\'" . gmat-script-mode))

;; Linting support
;; ***TO DO*** See https://gitlab.com/mlegendre/gitlab-ci-mode/-/blob/master/gitlab-ci-mode.el
;;             for an example of linting support for gitlab-ci-mode.

(provide 'gmat-script-mode)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:

;;; gmat-script-mode ends here
