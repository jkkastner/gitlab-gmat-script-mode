# gitlab-gmat-script-mode

'gmat-script-mode' is an Emacs major mode for editing GMAT (General Mission analysis Tool) script files. It provides syntax highlighting and completion for keywords and special variables.